(function($){
	var $this = $('.sudoku_frame');
	var eles = {
		table : $this,
		cells : '',
		selected: '',
		statusBar: $('div.status_bar'),
		selected: $('.selected')
	};

	var vars = {
		selected: {
			x : 0,
			y : 0
		},
		times: 0
	};
	var cell = {
		// Get the 3 by 3 groups given x and y OR the cell itself.
		getGroup: function(params) {
			if(typeof params.x !== "undefined" && typeof params.y !== "undefined") {
				var x = params.x, y = params.y;
				if(x>=1 && x<=3) {
					if(y>=1 && y<=3) {
						return 0;
					}
					else if(y>=4 && y<=6) {
						return 1;
					}
					else {
						return 2;
					}
				}
				if(x>=4 && x<=6) {
					if(y>=1 && y<=3) {
						return 3;
					}
					else if(y>=4 && y<=6) {
						return 4;
					}
					else {
						return 5;
					}
				}
				if(x>=7 && x<=9) {
					if(y>=1 && y<=3) {
						return 6;
					}
					else if(y>=4 && y<=6) {
						return 7;
					}
					else {
						return 8;
					}
				}
			}
			else if(typeof params.cell !== "undefined"){
				return $(params.cell).data('grp');
			}
		},
		// Get the row of the cell
		getRow: function(ele) {
			return $(ele).data('row');
		},
		// Get the column of the cell
		getCol: function(ele) {
			return $(ele).data('col');
		},
		// Select the cell
		select: function() {
			vars.selected.x = (vars.selected.x > 8) ? 0 : vars.selected.x;
			vars.selected.x = (vars.selected.x < 0) ? 8 : vars.selected.x;
			vars.selected.y = (vars.selected.y > 8) ? 0 : vars.selected.y;
			vars.selected.y = (vars.selected.y < 0) ? 8 : vars.selected.y;
			eles.cells.removeClass('selected');
			eles.selected = $(eles.cells[vars.selected.y * 9 + vars.selected.x]);
			eles.selected.addClass('selected');
		},
		// Fill only if it's not a fixed cell
		fill: function(ele, digit) {
			if(eles.selected.hasClass('fixed')) {
				return false;
			}
			else {
				eles.selected.find("span").html(digit);
			}
			if(eles.selected.hasClass('invalid')) {
				eles.selected.removeClass('invalid');
			}

			if(!(collection.validate(ele, digit)) {
				$(ele).addClass('invalid');
			}

			if( frame.validate() ) {
				eles.statusBar.html( "You Win! Congratulations!" ).show().delay(2500).fadeOut(2500);
			}
		},
		// Parse the element and get value for the cell.
		getValue: function(ele) {
			return $(ele).find(':first-child').text();
		}
	};
	var frame = {
		// Init a frame for now.
		initFrame: function() {
			for(var i = 9; i > 0; i--) {
				var tr = $('<tr></tr>');
				for(var j = 9; j > 0; j--) {
					var td = $('<td class="cell" data-row="'+(i-1)+'" data-col="'+(j-1)+'" data-grp="'+cell.getGroup({x:i,y:j})+'"><span></span></td>');
					if( !(((i >= 4 && i <= 6) || (j >= 4 && j <= 6)) && !(i >= 4 && i <= 6 && j >= 4 && j <= 6 ))) {
						// To mark the different squares with different colors.
						td.addClass('alt');
					}
					// To draw the lines.
					if(i % 3 === 0 ) {
						td.addClass('top_edge');
					}
					if(j % 3 === 0 ) {
						td.addClass('left_edge');
					}
					tr.append(td);
				}
				eles.table.append(tr);
			}
			eles.cells = eles.table.find('.cell');
			$(eles.cells[0]).addClass('fixed').find('span').html(5);
			$(eles.cells[1]).addClass('fixed').find('span').html(3);
			$(eles.cells[4]).addClass('fixed').find('span').html(7);
			$(eles.cells[9]).addClass('fixed').find('span').html(6);
			$(eles.cells[12]).addClass('fixed').find('span').html(1);
			$(eles.cells[13]).addClass('fixed').find('span').html(9);
			$(eles.cells[14]).addClass('fixed').find('span').html(5);
			$(eles.cells[19]).addClass('fixed').find('span').html(9);
			$(eles.cells[20]).addClass('fixed').find('span').html(8);
			$(eles.cells[25]).addClass('fixed').find('span').html(6);
			$(eles.cells[27]).addClass('fixed').find('span').html(8);
			$(eles.cells[31]).addClass('fixed').find('span').html(6);
			$(eles.cells[35]).addClass('fixed').find('span').html(3);
			$(eles.cells[36]).addClass('fixed').find('span').html(4);
			$(eles.cells[39]).addClass('fixed').find('span').html(8);
			$(eles.cells[41]).addClass('fixed').find('span').html(3);
			$(eles.cells[44]).addClass('fixed').find('span').html(1);
			$(eles.cells[45]).addClass('fixed').find('span').html(7);
			$(eles.cells[49]).addClass('fixed').find('span').html(2);
			$(eles.cells[53]).addClass('fixed').find('span').html(6);
			$(eles.cells[55]).addClass('fixed').find('span').html(6);
			$(eles.cells[60]).addClass('fixed').find('span').html(2);
			$(eles.cells[61]).addClass('fixed').find('span').html(8);
			$(eles.cells[66]).addClass('fixed').find('span').html(4);
			$(eles.cells[67]).addClass('fixed').find('span').html(1);
			$(eles.cells[68]).addClass('fixed').find('span').html(9);
			$(eles.cells[71]).addClass('fixed').find('span').html(5);
			$(eles.cells[76]).addClass('fixed').find('span').html(8);
			$(eles.cells[79]).addClass('fixed').find('span').html(7);
			$(eles.cells[80]).addClass('fixed').find('span').html(9);

			$(eles.cells[0]).addClass('selected');
			// Binding it here as cells is undefined on init
			eles.cells.on('click', function(){
				vars.selected.y = 8 - Number($(this).data('row'));
				vars.selected.x = 8 - Number($(this).data('col'));
				cell.select();
			})
			eles.selected = $(eles.cells[0]);
			eles.statusBar.hide();
		},
		// Allow only left, right, up, down and digits (except 0)
		handleKeyPress: function(event) {

			var e   = (!event) ? window.event : event;
			e.preventDefault();
			var key = e.which || e.keyCode;

			switch(key) {
				// UP
				case 38:
					vars.selected.y -= 1;
					cell.select();
				break;
				// RIGHT
				case 39:
					vars.selected.x += 1;
					cell.select();
				break;
				// LEFT
				case 37:
					vars.selected.x -= 1;
					cell.select();
				break;
				// DOWN
				case 40:
					vars.selected.y += 1;
					cell.select();
				break;

				// Numbers
				case 49:
				case 50:
				case 51:
				case 52:
				case 53:
				case 54:
				case 55:
				case 56:
				case 57:
					cell.fill($('.selected'), key - 48);
				break;
			}
		},
		// Check if any cell is blank
		checkBlanks: function() {
			for (var i = eles.cells.length - 1; i >= 0; i--) {
				if(cell.getValue(eles.cells[i]) === "") {
					return false;
				}
			};
			return true;
		},
		validate: function() {
			return (frame.checkBlanks() && $('.invalid').length === 0);
		}
	};
	var collection = {
		// Different collections and validations
		row : {
			validate: function(ele, digit) {
				// 8-vars.selected.y
				var cells = eles.table.find("[data-row='" + ele.data('row') + "']");
				var times = 0;
				var vals = [];

				for (var i = cells.length - 1; i >= 0; i--) {
					if(digit === Number(cell.getValue(cells[i]))) {
						times = times + 1;
					}
					if( times > 1 ) {
						return false;
					}
				};
				return true;
			}
		},
		column : {
			validate: function(ele, digit) {
				var cells = eles.table.find("[data-col='" + ele.data('col') + "']");
				var times = 0;
				var vals = [];

				for (var i = cells.length - 1; i >= 0; i--) {
					if(digit === Number(cell.getValue(cells[i]))) {
						times = times + 1;
					}
					if( times > 1 ) {
						return false;
					}
				};
				return true;
			}
		},
		group : {
			validate: function(ele, digit) {
				var cells = eles.table.find("[data-grp='" + ele.data('grp') + "']");
				var times = 0;
				var vals = [];

				for (var i = cells.length - 1; i >= 0; i--) {
					if(digit === Number(cell.getValue(cells[i]))) {
						times = times + 1;
					}
					if( times > 1 ) {
						return false;
					}
				};
				return true;
			}
		},
		validate: function(ele, digit) {
			return collection.row.validate(ele, digit) && collection.column.validate(ele, digit) && collection.group.validate(ele, digit);
		}
	};

	frame.initFrame();
	$(document).keyup(frame.handleKeyPress);

})(jQuery);